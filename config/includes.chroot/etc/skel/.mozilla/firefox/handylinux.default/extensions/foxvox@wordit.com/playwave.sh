#!/bin/bash

# get path to firefox extension where new wave file has been created
path=$1

aplay $path/speak.wav
error=$?
#echo "aplay err:$error"

if [ "$error" -ne "0" ]; then
{
	# Try with play
	play $path/speak.wav
	error=$?
	#echo "play err:$error"

    if [ "$error" -ne "0" ]; then
	{
	 ./bplay $path/speak.wav
	 error=$?
	 #echo "bplay err:$error"

		 if [ "$error" -ne "255" ]; then
		{
		#echo "bplay also failed. nothing played."
    	exit 1
    	} fi

	} fi

} fi

