var FoxVox = {
  onLoad: function() {
    // initialization code
    this.initialized = true;

//alert("startup");
FoxVox.FirstStartTasks();
  },

onMainCommand: function(event,foxvoxMode) {

var currentVoice = FoxVox.getPrefs("voice");
//alert("Got voice in main: " + currentVoice);

var extPath = FoxVox.getPrefs("extpath");


// ---- Define various command lines, which are then written to a shell script. ----
var newwin;
var data ="";
var space = " ";
var quote = '"';


// foxvoxMode is either: speak, or an audio format such as mp3,ogg,wav. This decides whether the resulting wave file is played back, saved to a user defined file, or compressed and then saved to a user defined file.

//alert(foxvoxMode);

	// Get the file path to save the audio file to if we are in an audiobook mode
	if ( (foxvoxMode == "wav") || (foxvoxMode == "mp3") || (foxvoxMode == "ogg") ) {

var nsIFilePicker = Components.interfaces.nsIFilePicker;
var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
fp.init(window, "Select a File", nsIFilePicker.modeSave);

fp.appendFilter("Audio Files","*.wav; *.mp3;*.ogg");

	var res = fp.show();
//alert("res:" + res);

	if (res == nsIFilePicker.returnOK || res == nsIFilePicker.returnReplace) {
	  var theFile = fp.file;
	  // get file path as string for shell script use and to show user
	  var audioFile = fp.file.path;
	  // leafName is just the filename without the path
	  var audioleaf = fp.file.leafName;
	//alert("Audio file path:" + audioFile);
	//alert("Audio leaf name:" + audioleaf);
	}

	// Not needed for wave file copy
	if (foxvoxMode != "wav") {

	// Open progress dialog
	var newwin = window.openDialog("chrome://foxvox/content/progress.xul","progressWin" , "chrome,centerscreen");
	}
}

// get platform (win or lin)
var platform = navigator.platform;

// -------- Windows-specific commands -----------
if (platform.match(/Win32/i) ) {
//alert('Windows');

var speak_binary = "espeak\.exe";
var mbrola_binary = "mbrola\.exe";
var shell_script = "speaknow\.bat";
//var shell_script_shortcut = "speaknow\.bat\.lnk";
var separator = "\\";
var nl = "\r\n";

// In windows no shell binary, the batch file is called directly
var launchShell = extPath + separator + "speaknow\.vbs";
var shellScriptPath = extPath + separator + shell_script;
//alert("win launch:" + launchShell);

	// ----- speak text windows ------------
	// command to play wave file on windows
	if (foxvoxMode == "speak") {
	var audioCommand = quote +extPath + separator + "wv_player\.exe" + quote + space + quote + extPath + separator + "speak\.wav" + quote;
	}

	// --------- wav copy windows -----------
	if (foxvoxMode == "wav") {
	// test for .wav extension in specified filename
	var regex = /.*\.wav$/;
	var result = regex.test(audioFile);
	//alert(result);

        // add extension if user did not enter it
        if ( result == false ) {
        audioFile = audioFile + "\.wav";
        //alert("audioFile:" + audioFile);
        }

	var audioCommand = "copy" + space + quote + extPath + separator + "speak\.wav" + quote + space + quote + audioFile + quote;

    var audiodoneFile = extPath + separator + "wavdone\.txt";
	var noEncoderFile = extPath + separator + "nowav\.txt";
    //alert("win wav noEncoderFile:" + noEncoderFile);
    }

	// ----- mp3 windows ----------
	if (foxvoxMode == "mp3") {
	// test for .mp3 extension in specified filename
	var regex = /.*\.mp3$/;
	var result = regex.test(audioFile);
	//alert(result);

	// add extension if it user did not enter it
	if ( result == false ) {
	audioFile = audioFile + "\.mp3";
	//alert("audioFile:" + audioFile);
	}

	var audioCommand =quote + extPath + separator + "createmp3\.bat" + quote + space + quote + extPath + quote + space + quote + audioFile + quote;

	var audiodoneFile = extPath + separator + "mp3done\.txt";
	var noEncoderFile = extPath + separator + "nolame\.txt";
}

	// ------- ogg windows --------------
	// test for .ogg extension in specified filename
		if (foxvoxMode == "ogg") {
	var regex = /.*\.ogg$/;
	var result = regex.test(audioFile);
	//alert(result);

	// add extension if it user did not enter it
	if ( result == false ) {
	audioFile = audioFile + "\.ogg";
	//alert("audioFile:" + audioFile);
	}

	var encoderPath = quote + extPath + separator + "oggenc2\.exe" + quote;

	var audioCommand = quote + extPath + separator + "createogg\.bat" + quote + space + encoderPath + space + quote + extPath + quote + space + quote + audioFile + quote;

	var audiodoneFile = extPath + separator + "oggdone\.txt";

	var noEncoderFile = extPath + separator + "noogg\.txt";
}

var delMarkerFiles = extPath + separator + "delmarkerfile\.vbs";
//alert("delMarkerFiles: " + delMarkerFiles);
}


// ------- Linux-specific commands --------
if (platform.match(/Linux/i) ) {
//alert('Linux');
var speak_binary = "speak";
var mbrola_binary = "mbrola-linux-i386";
var separator = "/";

// audio mode
	if (foxvoxMode == "speak") {
	var audioCommand = "/bin/sh " + extPath + separator + "playwave\.sh" + space + extPath;
	}

	if (foxvoxMode == "wav") {
	// test for .wav extension in specified filename
	var regex = /.*\.wav$/;
	var result = regex.test(audioFile);
	//alert(result);

	// add extension if it user did not enter it
	if ( result == false ) {
	audioFile = audioFile + "\.wav";
	//alert("audioFile:" + audioFile);
	}

	var audioCommand = "/bin/sh " + extPath + separator + "createwav\.sh" + space + extPath + space + audioFile;

	var audiodoneFile = extPath + separator + "wavdone\.txt";

	var noEncoderFile = extPath + separator + "nowav\.txt";
    }

	if (foxvoxMode == "mp3") {
	// test for .mp3 extension in specified filename
	var regex = /.*\.mp3$/;
	var result = regex.test(audioFile);
	//alert(result);

	// add extension if it user did not enter it
	if ( result == false ) {
	audioFile = audioFile + "\.mp3";
	//alert("audioFile:" + audioFile);
	}

	var audioCommand = "/bin/sh " + extPath + separator + "createmp3\.sh" + space + extPath + space + audioFile;
	var audiodoneFile = extPath + separator + "mp3done\.txt";
	var noEncoderFile = extPath + separator + "nolame\.txt";
}

	// test for .ogg extension in specified filename
	if (foxvoxMode == "ogg") {
	var regex = /.*\.ogg$/;
	var result = regex.test(audioFile);
	//alert(result);

	// add extension if it user did not enter it
	if ( result == false ) {
	audioFile = audioFile + "\.ogg";
	//alert("audioFile:" + audioFile);
	}

	var audioCommand = "/bin/sh " + extPath + separator + "createogg\.sh" + space + extPath + space + audioFile;
	var audiodoneFile = extPath + separator + "oggdone\.txt";
	var noEncoderFile = extPath + separator + "noogg\.txt";
}

var shell_script = "speaknow\.sh";

// path to shell
var launchShell = "/bin/sh";
var shellScriptPath = extPath + separator + shell_script;

// On Linux we need to add the directory to lib path before using espeak so that  libportaudio can be found if not installed on system. Achieved by exporting path before calling application.
var exportLibPath = "export LD_LIBRARY_PATH=" + extPath + ':$LD_LIBRARY_PATH;';

//define shell script in data var
var shebang = "#!/bin/sh";
var nl = "\n";
var data = shebang +nl + nl;
var data = data + exportLibPath;

var delMarkerFiles = extPath + separator + "delmarkerfile\.sh";
}

//alert("delMarkerFiles: " + delMarkerFiles);

// enumerate installed voices and create menus
// this really only needs doing once per session
//var voicesPath = extPath + separator +"mbrola";
//FoxVox.setupVoices(voicesPath);


// ----- Launch a script to delete the marker files or encoding will not proceed ---------

// create an nsILocalFile for the executable
var launchDelFile = Components.classes["@mozilla.org/file/local;1"]
                     .createInstance(Components.interfaces.nsILocalFile);
launchDelFile.initWithPath(delMarkerFiles);

if ( launchDelFile.exists() == true ) {

// create an nsIProcess
var process = Components.classes["@mozilla.org/process/util;1"]
                        .createInstance(Components.interfaces.nsIProcess);
process.init(launchDelFile);

// Run the shell script as a process.
var args = [extPath];
process.run(false, args, args.length);
}
else {alert("no del file");}

// ---- Check if first launch and open home page and save marker file if it is ----

    if (parseInt(FoxVox.getPrefs("counter")) == 1) {

    alert("Welcome to FoxVox.\n\nKeyboard Shortcuts:\n\nF9 or F12 start speech\nShift+F9 or Shift+F12 stop speech\n\nNote: sometimes other extensions may already be using those keys,\nthat is why there are two options available.");

	// Add tab
	//gBrowser.selectedTab =
	gBrowser.addTab("http://wordit.com");

    // increment counter
    var newcounter = parseInt(FoxVox.getPrefs("counter"))+1;
    FoxVox.setPrefs("counter", newcounter);
	}

    if (parseInt(FoxVox.getPrefs("counter")) == 10) {

    alert("The most requested FoxVox feature has been better sounding voices.\nThere are better sounding commercial voices available.\n\nFoxVox Pro can use the high-quality voices by Cepstral.\nI will now open a page with more information.\n\n");

	// show VF info window
	FoxVox.vfinfo();

    // increment counter
    var newcounter = parseInt(FoxVox.getPrefs("counter"))+1;
    FoxVox.setPrefs("counter", newcounter);
	}

    if ((parseInt(FoxVox.getPrefs("counter")) == 50) || (parseInt(FoxVox.getPrefs("counter")) == 100)) {

    alert("Hi, you have used FoxVox frequently.\nPlease consider making a donation to support development,\nor upgrading to the Pro version.\n\nThanks for supporting FoxVox.\n\n");

	// show VF info window
	FoxVox.vfinfo('donate');

    // increment counter
    var newcounter = parseInt(FoxVox.getPrefs("counter"))+1;
    FoxVox.setPrefs("counter", newcounter);
	}

// On the with the show...

// Get the selected text
var selectedText = document.commandDispatcher.focusedWindow.getSelection().toString();
	if (selectedText) {
		//alert(selectedText);

// Only do TTS if text was selected

// Get text length and show a message for longer texts informing user that there is currently no way to stop the process.

var selectedTextLength = selectedText.length;
//alert("selectedTextLength: " + selectedTextLength);

// ---- write out file called speak.txt containing selectedText ----

// selected text is written to a file and passed to espeak
var textToSpeakFile = extPath + separator + "speak\.txt";

// create an nsILocalFile for writing to
var speakFile = Components.classes["@mozilla.org/file/local;1"]
                     .createInstance(Components.interfaces.nsILocalFile);

speakFile.initWithPath(textToSpeakFile);

// speakfile is nsIFile, data is a string
var foStream = Components.classes["@mozilla.org/network/file-output-stream;1"]
.createInstance(Components.interfaces.nsIFileOutputStream);

//init the file
// use 0x02 | 0x10 to open file for appending.
foStream.init(speakFile, 0x02 | 0x08 | 0x20, 0777, 0);
// write file
foStream.write(selectedText, selectedText.length);
foStream.close();

// --- strings to define for shell script
var espeakBinPath = quote + extPath + separator + speak_binary + quote;
//alert("espeak Path:" + espeakBinPath);

var mbrolaBinPath = quote + extPath + separator + mbrola_binary + quote;
//alert("mbrola bin:" + mbrolaBinPath);

var mbrolaVoice = currentVoice + separator + currentVoice;
//alert("mbrolaVoice:" + mbrolaVoice);

var mbrolaDataPath = quote + extPath + separator + "mbrola" + separator + mbrolaVoice + quote;
//alert("mbrolaDataPath:" + mbrolaDataPath);

var espeakVoice = " -vmb-" + currentVoice + space;
var espeakOptions = " -s 130 ";
var mbrolaOptions = " -e ";

//espeak part
var data = data + espeakBinPath + space + "--path=" + quote + extPath + quote + space + espeakVoice + space + "--phonout=" + quote + extPath + separator + "speak.pho" + quote + space + "-f" + space + quote + textToSpeakFile + quote + nl + nl;

//mbrola part
var data = data + mbrolaBinPath + space + mbrolaOptions + space + mbrolaDataPath + space + quote +extPath + separator + "speak.pho" + quote + space + quote + extPath + separator + "speak.wav" + quote + nl + nl;

// add the audio command to either play the created sound file, save it, or encode it in mp3 or ogg format
var data = data + audioCommand + nl + nl;

// show data
//alert("data:" + data);

//alert("script args:" + shellScriptPath);


// --- create and write shell script -------

//write script to a file. script is then run.
// create an nsILocalFile for writing to
var scriptFile = Components.classes["@mozilla.org/file/local;1"]
                     .createInstance(Components.interfaces.nsILocalFile);
scriptFile.initWithPath(shellScriptPath);

// scriptFile is nsIFile, data is a string
var foStream = Components.classes["@mozilla.org/network/file-output-stream;1"].createInstance(Components.interfaces.nsIFileOutputStream);

//init the file
// use 0x02 | 0x10 to open file for appending.
foStream.init(scriptFile, 0x02 | 0x08 | 0x20, 0755, 0);
// write file
foStream.write(data, data.length);
foStream.close();


function launchMainScript() {
//alert('launch main extpath'+extPath);
//alert("mode:"+foxvoxMode);
// ----- Launch shell script ---------

// create an nsILocalFile for the executable
var launchFile = Components.classes["@mozilla.org/file/local;1"]
                     .createInstance(Components.interfaces.nsILocalFile);
launchFile.initWithPath(launchShell);

// create an nsIProcess
var process = Components.classes["@mozilla.org/process/util;1"]
                        .createInstance(Components.interfaces.nsIProcess);
process.init(launchFile);

// Run the shell script as a process.
// If first param is true, calling thread will be blocked until
// called process terminates.
// Second and third params are used to pass command-line arguments
// to the process.
var args = [shellScriptPath];
process.run(false, args, args.length);

	// Exit function if not generating output files
	if (foxvoxMode == "speak") {
	return;
	}
    
    if (foxvoxMode == "wav") {
    // end interval checking loop
	alert("Your new audiobook is now ready at:\n\n" + audioFile + "\n");
    return;
    }

//--- wait for new audio file creation ----
//alert("noEncoderFile: "+noEncoderFile);

// check no encoder error
// create an nsILocalFile for writing to
var noEncoderMarker = Components.classes["@mozilla.org/file/local;1"]
                     .createInstance(Components.interfaces.nsILocalFile);
noEncoderMarker.initWithPath(noEncoderFile);

	if ( noEncoderMarker.exists() == true ) {

        // close progress meter window
        if (newwin) {
        newwin.close();
        }

	alert("Sorry, I could not find the " + foxvoxMode + " audio encoder. I will open a new page with instructions on installing the audio encoders for mp3 and ogg vorbis.");

	// Add tab
	//gBrowser.selectedTab =
	gBrowser.addTab("http://wordit.com");
	}
	else {
		//alert(foxvoxMode + " encoder is present" + "audiodoneFile:" + audiodoneFile);
    //alert("audiodoneFile:" + audiodoneFile);

// create an nsILocalFile for writing to
var audiodoneMarker = Components.classes["@mozilla.org/file/local;1"]
                     .createInstance(Components.interfaces.nsILocalFile);
audiodoneMarker.initWithPath(audiodoneFile);
//alert("Created audiodone File:" + audiodoneFile);

// ----- use a setInterval loop to check for the marker file indicating that encoding has completed. A regular while statement freezes the browser. ----

var intervalID;

function checkEncoding(intervalID) {

	if ( audiodoneMarker.exists() == true ) {

        // close progress meter window
        if (newwin) {
        newwin.close();
        }

	// end interval checking loop
	clearWait();

	alert("Your new audiobook is now ready at:\n\n" + audioFile + "\n");
	}

	//else{ alert("not yet...");}
}

function waitEncoding()
{
  intervalID = window.setInterval(checkEncoding, 500);

}

function clearWait() {
clearInterval(intervalID);
}

waitEncoding();

}

}

function delayedLaunch(interval)
{
	//alert("interval: " + interval);
  timeoutID = window.setTimeout(launchMainScript, interval);
}

// set interval to zero if speaking because no need to wait for progress meter GUI
if (foxvoxMode == "speak") {
delayedLaunch(0);
}
else {
delayedLaunch(1000);
}

// End TTS
	}

	// Tell user could not speak because had no text
	else {
		alert("You have not selected any text for me to speak.");
	}

  },


getPrefs: function(prefName) {
//alert("prefName" + prefName);

// Get the "extensions.myext." branch
var prefs = Components.classes["@mozilla.org/preferences-service;1"].
                    getService(Components.interfaces.nsIPrefService);
prefs = prefs.getBranch("foxvox.");

// prefs is an nsIPrefBranch.
// Look in the above section for examples of getting one.
var value = prefs.getCharPref(prefName); // get a pref
//alert("val:" + value);

return(value);
},


setPrefs: function(prefName,prefVal) {
//alert("prefName, prefVal" + prefName + ", " + prefVal);

// Get the "extensions.myext." branch
var prefs = Components.classes["@mozilla.org/preferences-service;1"].
                    getService(Components.interfaces.nsIPrefService);
prefs = prefs.getBranch("foxvox.");

// prefs is an nsIPrefBranch.
prefs.setCharPref(prefName, prefVal); // set a pref
},


onSetVoiceCommand: function(voice) {
//alert("setting voice:" + voice);
FoxVox.setPrefs("voice",voice);

var newval = FoxVox.getPrefs("voice");
//alert("voice pref is now:" + newval);

// set the checkmarks on both menus
FoxVox.setVoiceCheckmarks();

},


// sets up voices menu on FF launch
FirstStartTasks: function() {
//alert("FirstStartTasks");

var newcounter = parseInt(FoxVox.getPrefs("counter"))+1;
//alert("new counter:"+newcounter);
FoxVox.setPrefs("counter", newcounter);

// get platform (win or lin)
var platform = navigator.platform;

if (platform.match(/Win32/i) ) {
var separator = "\\";
}

if (platform.match(/Linux/i) ) {
var separator = "/";
}

    // set extension path
    //~ if (FoxVox.getPrefs("extpath") != "") {
    //~ //alert("prefs extpath:"+FoxVox.getPrefs("extpath"));
    //~ var voicesPath = FoxVox.getPrefs("extpath") + separator +"mbrola";
    //~ //alert("voicesPath:"+voicesPath);
    //~ FoxVox.setupVoices(voicesPath);
    //~ }
    //~ else {
        // get the profile directory, then add rest of path
     var extension = Components.classes["@mozilla.org/file/directory_service;1"].  getService(Components.interfaces.nsIProperties).get("ProfD", Components.interfaces.nsIFile);  

    // NOTE: "file" is an object that implements nsIFile. path is in file.path
    extension.append("extensions"); 
    extension.append("foxvox@wordit.com");

    //get extension path
    var extPath = extension.path;
    FoxVox.setPrefs("extpath", extPath);
    var voicesPath = extPath + separator +"mbrola";
    //alert("voicesPath:"+voicesPath);
    FoxVox.setupVoices(voicesPath);
    //alert("set path:"+extPath);
    //~ }

// end FirstStartTasks
},


// get voices and add to list
setupVoices: function(voicesPath) {
//alert("voicesPath:" + voicesPath);

var file = Components.classes["@mozilla.org/file/local;1"]
.createInstance(Components.interfaces.nsILocalFile);
file.initWithPath(voicesPath);

 //file is the given directory (nsIFile)
var entries = file.directoryEntries;
var voicesArray = [];
	while(entries.hasMoreElements())
	{
	  var entry = entries.getNext();
	  entry.QueryInterface(Components.interfaces.nsIFile);
	  voicesArray.push(entry);
	  //alert("file:" + entry.path);
	}

	for(var i = 0; i < voicesArray.length; i++) {
	//alert("file:" + voicesArray[i].leafName);

	// add menu item for voices
	  var vMenu = document.getElementById("vpop-menu");
	  var voicesMenuitem = document.createElement("menuitem");
	// construct a string for the command and new arg
	var thisComm = "FoxVox.onSetVoiceCommand('" +voicesArray[i].leafName + "');";
	//alert("thiscomm: " + thisComm);
	var thisId = voicesArray[i].leafName + "-menu";
	//alert("thisId " + thisId);
	  voicesMenuitem.setAttribute("id",thisId);
	  voicesMenuitem.setAttribute("label",voicesArray[i].leafName+'      ');
	  voicesMenuitem.setAttribute("type","radio");
	  voicesMenuitem.setAttribute("oncommand", thisComm);
	  vMenu.appendChild(voicesMenuitem);


	  //--------------------------

	  // add menu item for voices
	  var vMenu2 = document.getElementById("vpop-context-menu");
	  var voicesMenuitem2 = document.createElement("menuitem");
	// construct a string for the command and new arg
	var thisComm2 = "FoxVox.onSetVoiceCommand('" +voicesArray[i].leafName + "');";
	//alert("thiscomm: " + thisComm);
	var thisId2 = voicesArray[i].leafName + "-menu2";
	//alert("thisId2 " + thisId2);
	  voicesMenuitem2.setAttribute("id",thisId2);
	  voicesMenuitem2.setAttribute("label",voicesArray[i].leafName+'      ');
	  voicesMenuitem2.setAttribute("type","radio");
	  voicesMenuitem2.setAttribute("oncommand", thisComm2);
	  vMenu2.appendChild(voicesMenuitem2);
	}

FoxVox.setVoiceCheckmarks();

},


// set the checkmarks on both menus
setVoiceCheckmarks: function() {
// checkmark preferred voice

// tools menu
var currentVoice = FoxVox.getPrefs("voice");
var voiceString = currentVoice + "-menu";
//alert("voiceString: " + voiceString);
// get menuitem
var selectedVoiceMenuitem = document.getElementById(voiceString);
// checkmark it
selectedVoiceMenuitem.setAttribute("checked", "true");

// ------------------------

// context menu
var currentVoice2 = FoxVox.getPrefs("voice");
var voiceString2 = currentVoice2 + "-menu2";
//alert("voiceString: " + voiceString);
// get menuitem
var selectedVoiceMenuitem2 = document.getElementById(voiceString2);
// checkmark it
selectedVoiceMenuitem2.setAttribute("checked", "true");

},


StopSpeech: function() {
//alert("stopping speech");

var extPath = FoxVox.getPrefs("extpath");

// get platform (win or lin)
var platform = navigator.platform;

if (platform.match(/Win32/i) ) {

var launchShell = extPath + "\\" + "stopspeech\.vbs";

// Windows needs path parameter in quotes due to possible spaces
var shellScriptPath = '"' + extPath + '"';

//alert("win: launchShell shellScriptPath: " + launchShell + " :" + shellScriptPath + ":");

// check for taskkill error
//~ var noTaskkillFile = extPath + "\\notaskkill.txt";

}


if (platform.match(/Linux/i) ) {
var launchShell = "/bin/sh"
var shellScriptPath = extPath + "/stopspeech\.sh";

//alert("launchShell shellScriptPath: " + launchShell + " " + shellScriptPath);
}

// ----- Launch shell script ---------

// create an nsILocalFile for the executable
var launchFile = Components.classes["@mozilla.org/file/local;1"]
                     .createInstance(Components.interfaces.nsILocalFile);
launchFile.initWithPath(launchShell);

// create an nsIProcess
var process = Components.classes["@mozilla.org/process/util;1"]
                        .createInstance(Components.interfaces.nsIProcess);
process.init(launchFile);

var args = [shellScriptPath];
process.run(false, args, args.length);

	//~ if (platform.match(/Win32/i)) {
        //~ // Check whether tskill was available on Windows. If not, show message.
        //~ var File = Components.classes["@mozilla.org/file/local;1"]
                     //~ .createInstance(Components.interfaces.nsILocalFile);
        //~ File.initWithPath(noTaskkillFile);
//~ 
	//~ alert( "I could not stop speech output because I could not find the Taskkill ('tskill') program. The file is either on your Windows installation disk, or you can download it from the Microsoft website. tskill for XP is included in the 'XP Service Pack 1'. Check the Microsoft website for further information." );
	// Add tab
	//gBrowser.selectedTab =
	//	gBrowser.addTab("http://wordit.com");
	//~ }

//else { alert( "tskill OK" ); }

},


vfinfo: function(option) {
var url;

if (option == 'donate') {
url = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=U95UBG3ZNHAFY";
}
else {
url = "http://wordit.com";
}

var thisWidth = window.innerWidth-50;
var thisHeight = window.innerHeight-50;
//alert(thisWidth + "x" + thisHeight);

var win = window.open(url, "FoxVox Info", "width="+thisWidth+",height="+thisHeight+",resizable,scrollbars=yes,status=no");
//alert(win);

// stop any TTS speech so it doesnt talk over the voice samples. wait a sec for window to open though.
function delayedStop() {
timeoutID = window.setTimeout(FoxVox.StopSpeech, 1000);
}

delayedStop();
},


};

window.addEventListener("load", function(e) { FoxVox.onLoad(e); }, false);
