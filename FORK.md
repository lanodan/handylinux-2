== Modifications apportés à cette fourche ==
* Authentification automatique et verouillée sur : eleves
* Firefox sans conversation d’historique
* "Menu des applications" ajouté à coté du handymenu
* Paquets en plus pour
	* Impression réseau
	* Administration
