HOW TO build HandyLinux
=======================
cette page relate le processus complet de construction d'HandyLinux  
ces sources permettent de construire les deux versions i386 et amd64.

mode d'emploi : suivez le tuto jusqu'au clonage des sources, puis affichez les sources à coté du tuto pour visualiser rapidement la structure du build et le rôle de chaque fichier.  
amusez-vous bien :)

need help ? [mailme](arpinux@member.fsf.org)

------------------------------------------------------------------------

Introduction
============
HandyLinux est construite sur 3 ressources :

 - les paquets Debian présents dans les dépôts du sources.list,
 - les paquets du dépôt HandyLinux,
 - les sources du build que vous consultez actuellement, à utiliser avec le programme **live-build**.

les paquets sont appelés pas le fichier /config/package-lists/handylinux.list.chroot .  
les sources modifient 'en dur' la construction de l'image ISO, soit en intégrant des fichiers de configuration, soit en modifiant la construction grâce à certains scripts.

------------------------------------------------------------------------

Principe du live-cd
===================
HandyLinux est distribuée sous forme d'image ISO destinée à être gravée ou transférée sur une clé USB afin de créer un 'live-cd'.  
un live-cd est constitué d'une image système (toute l'architecture embarquée d'une installation) compressée dans une archive de type "squashfs" et d'un programe de boot permettant de lancer ce système compressé.  
ce programme de boot peut être accompagné d'un installeur, permettant ainsi de reporter le système compressé sur un disque dur. dans ce cas, c'est un live-cd-installable.  
sur HandyLinux, on utilise **live-build**, le programme de construction officiel des images disque Debian

------------------------------------------------------------------------

Principe du live-build
======================
live-build est constitué d'une série de scripts destinés à construire une image live et/ou installable d'un système Debian.  
ces scripts acceptent des arguments permettant de personnaliser l'image finale.  
les arguments peuvent être réunis dans des scripts qui sont placés dans le dossier 'auto' des sources.

processus de construction
-------------------------
- lorsqu'on lance la commande de construction, live-build lit les instructions que vous avez laissé.  
- live-build détecte l'architecture, le noyau à utiliser etc, puis il utilise debootsrap pour créer un chroot de base.  
- ensuite il complète l'installation dans le chroot avec les paquets listés dans le /config/package-lists/blah.list.chroot .  
- tous les paquets sont mis en cache dans un dossier qui se créé au moment de la construction directement à la racine des sources (/cache).  
- une fois le chroot installé, live-build va reporter le contenu de /config/includes.chroot/ (votre personnalisation du système) dans le chroot.  
- viennent ensuite les 'hooks', le(s) script(s) qui vont modifier le chroot juste avant de le démonter.  
- puis live-build démonte le chroot et commence à le compresser en  squashfs (la partie la plus gourmande et la plus longue).  
- enfin, il va chercher les paquets nécessaires pour l'installeur et la création de l'iso (isolinux, xorriso, etc) et génère l'image ISO.  

... en très gros :D

------------------------------------------------------------------------

Mise en place
=============
Installation des dépendances
----------------------------
HandyLinux se sert programme de construction par défaut chez Debian, **live-build**.  
Pour ses sources, c'est le protocole **git** qui est utilisé :

	sudo apt-get update && sudo apt-get install live-build live-manual live-tools git apt-cacher-ng

Clonage des sources
-------------------
Pour utiliser et modifier les sources localement, vous devez les cloner sur votre système.  

	git clone https://git.framasoft.org/handylinux/handylinux-2.git

Vous obtenez alors un dossier *handylinux-2* qui contient l'intégralité des sources HandyLinux.  

------------------------------------------------------------------------

Contenu des sources du build
============================

liste des dossiers et fichiers des sources HandyLinux-2.x  
aka *koya dedans... kesafula ??*

------------------------------------------------------------------------

- /Makefile  
script de lancement de la construction.  
permet de construire les deux versions i386 et amd64 en précisant le type de construction en argument.
lancer 'make' pour la liste des options disponibles.
- /README.md  
fichier de présentation générale HandyLinux.  
ce fichier contient aussi l'intégralité du changelog par versions.
- /TODO.md  
les trucs à faire.
- /VERSION.md  
fichier indiquant la version courante des sources et la date de sortie.
- /doc  
documentation et tutoriel de construction.

------------------------------------------------------------------------

**/addons/32/** : dossier contenant les ficiers spécifiques pour une HandyLinux i386.  
ces données sont copiées par le Makefile lors du lancement du build.  
les données sont nettoyées du build lors du clean ! à faire obligatoirement entre deux builds d'architectures différentes !

- 586-kernel-cleaner  
script de nettoyage du kernel 586 en cas d'installation sur un système acceptant le 686-pae.  
ce script est copié par le Makefile lors de la construction d'un i386.  
ce script est appelé par le /etc/rc.local du système installé lors du premier boot,  
permettant ainsi la suppression du noyau inutile sans l'intervention de l'utilisateur.
- handylinux_preseed  
script de post-installation automatique. version fr.  
ce script est copié par le Makefile lors de la construction dans /config/includes.chroot/usr/local/bin/ .  
ce script est appelé en fin d'installation et permet d'appliquer les dernières modifications  
avant de démonter le système installé. il est effacé après exécution.
- handylinux_preseed-en  
script de post-installation automatique. version en.  
idem version fr, mais si la langue d'installation est autre que 'fr'.
- live32.cfg  
fichier de configuration du menu isolinux live 32b version fr.  
ce fichier est copié par le Makefile lors de la construction dans /config/includes.binary/isolinux/live.cfg .
- live32-en.cfg  
fichier de configuration du menu isolinux live 32b version en.  
ce fichier est copié par le Makefile lors de la construction dans /config/includes.binary/isolinux/live-en.cfg .
- rc.local.kc  
rc.local."kernel-cleaner" qui lance le script de nettoyage 586-kernel-cleaner en cas d'installation sur une proc 686-pae.  
ce fichier est copié par le Makefile lors de la construction dans /config/includes.chroot/usr/share/handylinux/ .
- rc.local.orig  
rc.local qui est copié après le premier boot et l'exécution du 586-kernel-cleaner pour rétablir la config Debian par défaut.  
ce fichier est copié par le Makefile lors de la construction dans /config/includes.chroot/usr/share/handylinux/ .

------------------------------------------------------------------------

**/addons/64** : idem '/addons/32' mais pour HandyLinux amd64.  
moins de fichiers car il n'y a pas de nettoyage de noyau à faire.

- handylinux_preseed  
script de post-installation automatique. version fr.
- handylinux_preseed-en  
script de post-installation automatique. version en.
- live64.cfg  
fichier de configuration du menu isolinux live 64b version fr.
- live64-en.cfg  
fichier de configuration du menu isolinux live 64b version en.

------------------------------------------------------------------------

**/auto/** : contient les scripts de construction et de nettoyage pour les commandes 'lb config', 'lb build' et 'lb clean'.  
ces scripts permettent de passer en arguments de façon plus claire, les options pour live-build.

- build  
script de construction qui va lire le fichier 'config' et porter le retour du build dans un log.
- clean  
script de nettoyage du dossier de construction.
- config32  
fichier de configuration pour live-build version 32b. sera copié en '/auto/config' par le Makefile.
- config64  
fichier de configuration pour live-build version 64b. sera copié en '/auto/config' par le Makefile.

------------------------------------------------------------------------

**/config/archives** : dossier contenant les adresses des dépôts externes à utiliser ainsi que leurs clés d'authentification.  
ces données seront copiées automatiquement dans le dossier /etc/apt/sources.list.d/ du système installé et les clés ajoutées à apt.

- .key.binary  
clé d'authentification handylinux et mozilla pour le livecd.
- .key.chroot  
clé d'authentification handylinuc et mozilla pour le chroot (qui sera le système installé).
- .list.binary  
sources.list additionnel utilisé pour le live.
- .list.chroot  
sources.list additionnel utilisé pour le chroot (qui sera le système installé).

------------------------------------------------------------------------

**/config/hooks** : dossier contenant les scripts de modification en cours de build.

- handylinux.chroot  
script de modification du build. ce script se lance en fin de construction, juste avant la compression du squashfs.  
il permet d'appliquer les dernières modifications et/ou de lancer les dernières commandes dans le chroot.  
  - mise en place de la configuration speechd pour orca : lancé dans le hook pour remplacer la configuration du paquet installé lors du build.
  - installation du paquet handylinux-desktop : lancé dans le hook pour éviter le bug du apt-get autoremove lié aux méta-paquets.
  - mise en place du lien skype : skype est présent dans le handymenu par défaut mais non-installé. pour éviter d'avoir à modifier le lanceur du handymenu,  
  on met un lien en place vers l'installeur (/usr/bin/skype-installer-launcher) qui a été installé lors du build.
  - mise en place du serveur DNS local : on le fait dans le hook, une fois le paquet unbound installé.
  - update-command-not-found : on le fait dans le hook car pour mettre à jour la liste des applications installées... bah il faut qu'elles soient installées ;)
- blah.chroot  
scripts mis en place par live-build. ce sont les "hooks" par défaut copié lors de la première procédure de build.  
c'est live-build direct qui s'en charge, on a pas à les modifier.

------------------------------------------------------------------------

**/config/includes.binary/install/** : fichiers intégrés dans le dossier 'install' du livecd.  

- hl-preseed-en.cfg  
fichier de configuration de l'installeur appelé par le menu isolinux version en.
- hl-preseed.cfg  
fichier de configuration de l'installeur appelé par le menu isolinux version fr.

------------------------------------------------------------------------

**/config/includes.binary/isolinux/** : fichiers intégrés dans le dossier 'isolinux' du livecd.  
les fichiers présents servent à la personnalisation du menu isolinux, le menu d'ouverture du liveDVD.  
ils seront complétés lors du build par les fichiers tirés du dossier 'addons' et copiés par le Makefile.

------------------------------------------------------------------------

**/config/includes.installer/usr/share/** : personnalisation de la bannière d'installation et de la couleur du thème gtk  
la bannière située dans ./graphics/logo_debian.png est automatiquement reconnue par live-build sous ce nom.  
le fichier de thème gtk situé dans ./themes/Clearlooks/gtk-2.0/gtkrc est modifié pour remplacer le 'rouge Debian' par du bleu ... ctout :P

------------------------------------------------------------------------

**/config/package-lists/** : dossier contenant les listes des paquets à installer.  
si vous ajoutez une liste maliste.live, les paquets contenus seront présents dans la session live mais seront supprimés lors de l'installation.

- handylinux.list.chroot  
liste des paquets par défaut pour HandyLinux les lignes '#' ne sont pas prises en compte.  
HandyLinux utilise l'option apt 'no-recommends' afin d'alléger le système :  
prenez soin de sélectionner les applications à intégrer en vérifiant les "paquets recommandés".
- live.list.chroot  
liste crée automatiquement par live-build afin d'intégrer les paquets live-build dans le liveDVD.

------------------------------------------------------------------------

**/config/includes.chroot** : tout ce qui sera placé dans ce dossier sera intégré dans le squashfs  
en respectant l'architecture et les permissions du système GNU/Linux de base.  
ex, un fichier placé dans /config/includes.chroot/etc/issue appartiendra à root dans le squashfs et sera situé dans /etc/issue

certains paquets handylinux-xxxx modifient également le système (ex: handylinuxlook contient les thèmes gtk et les fonds d'écran)

! attention ! rien de ce qui sera placé dans ce dossier en 'dur' ne pourra être mis à jour depuis le gestionnaire de paquets.

------------------------------------------------------------------------

**/config/includes.chroot/etc/dpkg/origins/**  
fichier normés pour Debian pour indiquer la page d'accueil du projet et l'adresse des reports de bugs.

**/config/includes.chroot/etc/skel/**  
dossier de configuration de l'utilisateur par défaut (skel=skeleton). ce dossier sera détaillé plus tard.

**/config/includes.chroot/etc/sudoers.d/**  
fichiers de personnalisation du prompt 'sudo' : permet d'afficher des ** ** pour le mot de passe et de traduire le prompt sudo en fr.

**/config/includes.chroot/etc/unbound/unbound.conf.d/unbound.conf**  
fichier de configuration du serveur DNS local mis en place lors du hook /config/hooks/handylinux.chroot .

**/config/includes.chroot/adduser.conf**  
fichier de configuration utilisé pour la création des nouveaux utilisateurs.  
ce fichier est supplanté par le configuration preseed lors de l'installation pour le premier utilisateur.  
les nouveaux utilisateurs créés ultérieurement ne font pas parti du groupe sudo et suivent ce fichier.

**/config/includes.chroot/bash.bashrc**  
fichier de configuration système de l'interpréteur bash ajusté pour tenir compte de command-not-found .

**/config/includes.chroot/issue /issue.net**  
login tty et ssh.

**/config/includes.chroot.slim.conf**  
fichier de configuration du gestionnaire de connexion SLIM.  
ce fichier est là pour fixer le thème par défaut sur handylinux.  
le thème slim handylinux est intégré au paquet handylinuxlook.

------------------------------------------------------------------------

**/config/includes.chroot/usr/local/bin/welcome /welcome-en** 
dossiers pour le message d'accueil handylinux.  
ils continnent aussi le script de la première mise à jour après installation 'handyupdate.py' appelé par ./welcome.sh

**/config/includes.chroot/usr/local/bin/welcome.sh**  
script lancé lors de la première connexion de la première installation.  
ce script met en place les dossiers utilisateurs définitifs (langue), les favoris, les actions personnalisées, lance le message d'acceuil, puis l'invite de mise à jour.

**/config/includes.chroot/usr/local/bin/welcome2.sh**  
script lancé lors e la première connexion des nouveaux utilisateurs.  
ce script a les mêmes fonctions que le précédent mais ne lance pas l'invite de mise à jour car les nouveaux utilisateurs ne font pas partie du goupe 'sudo' par défaut.

**/config/includes.chroot/usr/local/bin/keyboard_selector**  
script de choix du clavier lancé par ./welcome.sh en session live.

**/config/includes.chroot/usr/local/bin/orca-**  
scripts de lancement et de configuration ORCA ... vilain hack...

**/config/includes.chroot/usr/local/bin/touchpad-tap.sh**  
script qui force la 'tap-to-click' sur les ordinateurs portables

------------------------------------------------------------------------

**/config/includes.chroot/usr/share/applications/**  
lanceurs .desktop pour les scripts de /usr/local/bin/  
les lanceurs skype* et teamviewer* ne sont pas intégrés dans le paquet 'nonfree-installer' car ils doivent pouvoir être supprimés lors de l'installation de skype et/ou teamviewer.  
si on intègre les .desktop dans le paquet, apt va gueuler lors de la suppression du fichier.

**/config/includes.chroot/usr/share/handylinux/**  
dossier qui va accueillir l'initiation intégré (portée par handylinux-desktop). les autres fichiers seront déplacés lors de l'installation.  

- le 'orca-speech-config' sera copié lors de la post-installation dans /etc/speech-dispatcher/speechd.conf
- le sources.list sera copié dans /etc/apt/sources.list, juste pour avoir un fichier sans les entrées "cdrom" commentées
- update-notifier.desktop sera copié dans le dossier $HOME/.config/autostart de l'utilisateur par le script /usr/local/bin/welcome.sh afin d'automatiser la notification des mises à jour pour le premier utilisateur seulement.

**/config/includes.chroot/usr/share/icons/**  
dossier contenant le thème d'icône principal d'HandyLinux + les thèmes de curseur  
! attention ! pour intégrer un nouveau thème d'icône, penser à le compresser avant de l'importer, puis le décompresser dans /usr/share/icons/ afin de préserver les liens symboliques.

**/config/includes.chroot/usr/share/images/grub/handylinux.tga**  
image utilisée pour le fond du menu GRUB affiché après installation.  
la mise à jour du GRUB se fait lors de la post-installation  
 note : handylinux_pressed/handylinux_preseed-en copiés par le Makefile dans /config/includes.chroot/usr/local/bin/ et appelés par hl-preseed.cfg/hl-preseed-en.cfg)

------------------------------------------------------------------------

**/config/includes.chroot/etc/skel** : la configuration de l'utilisateur par défaut.

remarques générales :  

- les dossiers utilisateurs sont en français par défaut. ils sont traduis à la volée par le script /usr/local/bin/welsome(2).sh lors du premier lancement.
- le dossier /.mozilla contient la configuration de base du navigateur firefox.  
avant le dernier build (celui juste avant la sortie de la release), il faut le mettre à jour.  
procédure :
     - 1 - construire une image ISO handylinux (32 ou 64)
     - 2 - monter l'ISO dans vbox ou qemu
     - 3 - mettre à jour les addons firefox et vérifier le bon fonctionnement du navigateur
     - 4 - fermer le navigateur en prenant soin de vider l'historique/cache/cookies etc...
     - 5 - compresser le ~/.mozilla de la session live
     - 6 - récupérer l'archive .mozilla.txz et la décompresser dans les sources du build, à la place du .mozilla actuel
     - 7 - reconstruire l'ISO pour la sortie :)

contenu du dossier utilsateur par défaut :

- dossiers et fichiers cachés  
**/.config/Thunar/accels.scm** : raccourcis clavier pour Thunar
**/.config/autostart/** : lanceurs d'applications à démarrer automatiquement. les lanceurs présents ne sont pas actifs. le lanceur "welcome.desktop" sera supprimé lors de la première connexion.  
**/.config/Terminal/terminalrc** : préférences du terminal pardéfaut xfce4-terminal.  
**/.config/Mousepad/** : préférences de l'aditeur de texte Mousepad.  
**/.config/libreoffice/** : profil pour libreoffice4.  
**/.config/gtk-3.0 /gtk-2.0** : préférences GTKpar défaut pour l'utilisateur. les thèmes définis sont installés par handylinuxlook.  
**/.config/fontconfig** : préférences d'affichage de la police.  
**/.config/Clementine** : configuration du lectuer de musique Clementine.  
**/.config/xfce4/panel/** : lanceurs intégrés dans la barre de tâches xfce et leurs options.  
**/.config/xfce4/xfconf/xfce-perchannel-xml/** : configurations des applications du bureau XFCE (ristretto, le panel, le bureau, xfwm4 etc...).  
**/.config/mimeapps.list** : définition des applications par défaut selon le type de fichier.  
**/.config/Trolltech.conf** : fichiers de configuration de l'apparence des logiciels 'Qt' type VLC.  
**/.config/user-dirs.dirs  user-dirs.dirs.en** : définition des dossiers XDG par défaut. le fichier inutile sera supprimé lors de la première connexion.  
**/.config/user-dirs.locale** : définition de la langue des dossiers XDG. cefichier est modifié selon la langue à la première connexion.  
**/.gimp-2.8/** : profil pour l'interface simplifiée monofenêtre de Gimp.  
**/.local/share/ristretto/** : configuration supplémentaire pour ristretto, la visionneuse par défaut.  
**/.local/share/radiotray/bookmarks.xml** : liste des radios pour radiotray.  
**/.local/share/orca/** : pré-configuration d'ORCA.  
**/.mozilla/** : profil pour firefox (procédure de mise à jour décrite plus haut).  
**/.bashrc** : configuration bash pour l'utilisateur, définition du prompt et des alias.  
**/.gtk-bookmarks** : les favoris thunar, seront modifiés selon la langue si besoin lors dela première connexion.  
**/.redshift_location** : fichier de localisation pour redshift afin de définir un lieu par défaut.  
**/.xscreensaver** : préférences de l'économiseur d'écran.  

- dossiers et fichiers visibles  
**/Bureau/ReadMe LisezMoi** : fichier de bienvenue avec le lien vers la page d'accueil.  
**/Documents/Documentation** : lien vers /usr/share/handylinux/HandyLinux_Doc qui sera installé par handylinux-doc et handylinux-doc-en.  
**/Images/wallpapers** : lien vers le dossier système des fonds d'écran.  
**/Modèles/** : différents modèles pour le menu contextuel de thunar "créer un document...".  
**/Musique/Jamendo.desktop** : lanceur pour le site Jamendo.  
**/Public/** : dossier de partage par défaut contenant un mini tuto pour btshare.  
**/Téléchargements/handytri.desktop** : lanceur pour le handytri.  
**/Vidéos/** : vidéo officielle handylinux.  
**/guide.desktop** : lanceur pour l'initiation intégrée.

------------------------------------------------------------------------

**modification des sources depuis les paquets handylinux'**  
tous les fichiers intégrés par défaut dans le build et listés dans les chapitres précédents ne peuvent être mis à jour.  
afin d'intégrer des fichiers 'modifiables', il faut passer par des paquets debian : c'est un des rôles du dépôt handylinux.  
le dépôt contient aussi des applications au même titre que les dépôts Debian. leur contenu ne sera pas listé ici.

liste des fichiers ajoutés au build via les paquets du dépôt handylinux :  

- handy-update-notifier : en dehors des scripts de gestion des paquets, le handy-update-notifier intègre les préférences apt
  - /etc/apt/apt.conf.d/99synaptic : défini l'option recommends=false pour le GUI de apt, synaptic.
  - /etc/apt/apt.conf.d/00trustcdrom : défini l'option trustcdrom=true pour pouvoir accepter un cd de paquets Debian automatiquement.
  - /etc/apt/apt.conf.d/00recommends : défini recommends=false pour apt et aptitude.
  - /etc/apt/apt.conf.d/02periodic : défini la mise à jour des dépôts automatique=true afin d'assurer au handy-update-notifier des infos récentes.
- handylinux-desktop : contient les scripts d'aide et d'infos pour handylinux  
il contient aussi 'handylinux_version' qui sera placé dans /etc/handylinux_version, aux côtés de '/etc/handylinux_installation' créé lors de l'installation par handylinux_preseed.
- handylinux-doc & handylinux-doc-en : contient la documentation pdf d'HandyLinux qui sera placée dans /usr/share/handylinux/HandyLinux_doc  
le paquet inutile (selon la langue d'installation) sera supprimé par le script de post-installation handylinux_pressed
- handylinux-thunar-actions : contient les scripts nécessaires aux actions personnalisées mais aussi le fichier de config thunar qui sera placé dans /etc/skel/.config/Thunar/  
la configuration utile (selon la langue) sera sélectionnée lors de la première connexion du nouvel utilisateur depuis le script /usr/local/bin/welcome2.sh
- handylinuxlook : contient les thèmes et walls d'handylinux :
  - 2 thèmes pour Slim copiés dans /usr/share/slim/themes/
  - plusieurs fonds d'écran copiés dans /usr/share/xfce4/backdrops/
  - plusieurs thèmes GTK dont HandyLinuxClear/Dark copiés dans /usr/share/themes/
  - plusieurs icones copiées dans /usr/share/pixmaps pour les lanceurs intégrés dans le menu d'accessiblité ou le handymenu
  - le handytheme, script qui permet le changement rapide d'interface copié dans /usr/bin/

la modification de ces fichiers n'est donc pas possible depuis le build (sauf gros hack dans les hooks, mais c'est pas propre).  
il faut passer par les dépôts pour modifier ces fichiers ou ne pas installer les paquets listés pour éviter les conflits avec de probables ajouts 'en dur'... mais ce ne serait plus une handylinux :)

------------------------------------------------------------------------

Modification des sources
========================

ref les pdf live-build inclus ;)  
note : les pdf ont été réalisés avant la fusion des sources i386/amd64.  
je vais les mettre à jour... mais la procédure et les conseils restent valables :)

------------------------------------------------------------------------

Construction de L'ISO
=====================
La construction se lance depuis le Makefile intégré.  

	sudo make 32b		: construction de l'ISO i386 avec les noyaux 586 et 686-pae.
                          cette commande doit être suivi du clean associé avant une autre construction.
	sudo make clean32b	: nettoyage des sources de construction i386
                          à faire obligatoirement avant de construire une autre version.
	sudo make 64b		: construction de l'ISO amd64.
                          cette commande doit être suivi du clean associé avant une autre construction.
	sudo make clean64b	: nettoyage des sources de construction amd64
                          à faire obligatoirement avant de construire une autre version.
	sudo make cleanfull	: nettoyage complet du cache et des logs.

on obtient un fichier du type *binary.hybrid.iso*  
! attention ! il faut renommer ce fichier ISO en handylinux-2.3-amd64.iso (par ex) avant de lancer le 'clean' sinon vous perdrez votre travail ... :/

... et voilà :)

------------------------------------------------------------------------

des questions ?
---------------
@devs : posez vos questions ici ou en commentaires, ça étoffera le tuto :P

