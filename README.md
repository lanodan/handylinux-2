__pour déclarer un bug, proposer une amélioration ou contacter les devs du projet, rendez-vous [sur le forum](https://handylinux.org/forum) de la communauté.__

HANDYLINUX
==========

**HandyLinux** c'est l'accessibilité pour tous et la liberté pour chacun d'évoluer à son gré.

Basée sur **Debian GNU/Linux** avec **XFCE**, un environnement de bureau rapide, léger et stable, HandyLinux est **sûre, pratique et gratuite**.

Conçue pour faciliter l'accès à l'informatique à ceux qui débutent, les enfants, les seniors et ceux qui sont en quête de simplicité. Pour cela elle comprend :  
- un **pack simplifié d'applications** pour les tâches courantes (navigation sur le net, mail, suite office, logiciels photo/audio/vidéo) facilement accessibles depuis le **HandyMenu**.
- la **logithèque Debian** riche de milliers d'applications pour les aventuriers.
- une **aide complète** en ligne et dans la distribution ainsi qu'une initiation intégrée afin de répondre à vos questions et accompagner votre progression.
- Si vous êtes en situation de handicap léger, la rubrique "access" est là pour faciliter l'accès aux différentes fonctions de l'ordinateur : installation avec synthèse vocale, lecteur d'écran ORCA, zoom d'écran actif, sélectionneur rapide d'interface et les modules du navigateur : clavier virtuel et synthèse vocale.

----------

- [site principal](https://handylinux.org)
- [forum d'entraide](https://handylinux.org/forum)
- [documentation fr/en](https://handylinux.org/wiki)
- [blog du projet](http://blog.handylinux.org)
- [téléchargements](https://handylinux.org/wiki/doku.php/fr/download)

----------

- [web startpage](https://handylinux.org/start)
- [logos officiels](https://handylinux.org/artwork)
- [galerie handylinux](https://handylinux.org/wallpapers)
- [hébergement d'images](https://handylinux.org/fotoo)

----------

- [dépôts officiels](http://repo.handylinux.org)
- [sources et développement](https://handylinux.org/sources)
- [archives images ISO](http://arpinux.org/isos/handylinux/)
- [mail/contact](contact@handylinux.org)

----------

applications incluses
---------------------
- environnement de bureau : XFCE
- gestionnaire de fichiers : Thunar
- navigateur internet : Firefox
- client de messagerie : Icedove (thunderbird rebranded)
- clients torrent P2P : Transmission & BTshare
- lecteur vidéo : VLC
- lecteur audio : Clementine
- web radio : RadioTray
- visionneuse d'images : Ristretto & Cyclope
- éditeur d'images : the Gimp (interface simplifiée)
- suite bureautique : LibreOffice
- éditeur de texte : Mousepad
- gestionnaire d'archives : Xarchiver
- gestionnaire de logiciels simplifié : HandySoft
- recherche : Catfish
- aide à distance : TeamViewer-installer
- suite d'outils pour l'accessibilité : installation avec synthèse vocale, lecteur d'écran ORCA, zoom actif de bureau, clavier virtuel, menu simplifié
- gestionnaire d'impression et de numérisation
- aide française/anglaise complète en ligne et dans la distro, initiation intégrée et Centre d'aide simplifié

matériel requis
---------------
**HandyLinux** s'installe sur tout ordinateur moderne (PIV ou supérieur) pourvu de 512M de mémoire, nécessite 3,7 GB d'espace disque minimum.  
**HandyLinux** est distribuée en deux versions : i386 et amd64.

----------
